import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {User} from '../model/user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  password: string;
  name: string;
  type: number;
  address: string;
  birthDate: string;
  emailAddress: string;
  id: number;

  constructor(private router: Router, private userService: UserService) { }

  displayedColumns: string[] = ['id', 'name', 'type', 'address', 'birthDate', 'emailAddress'];
  users: any = [];
  ngOnInit() {
    this.id = Number(localStorage.getItem('editUserId'));
    console.log('ID USER:   ', this.id);
    this.userService.getUserById(this.id)
      .subscribe(data => {
        this.users = data;
        console.log('USER:  ', this.users);

      });
  }


  editUser(id, password, name, type, address, birthDate, emailAddress) {
    console.log('feom local storage id:  ', this.id);

    this.userService.editUser(this.id, password, name, type, address, birthDate, emailAddress);
  }
}
