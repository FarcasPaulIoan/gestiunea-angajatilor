import { Component, OnInit } from '@angular/core';
import {User} from '../model/user';
import {Router} from '@angular/router';
import {MeetingService} from '../services/meeting.service';

@Component({
  selector: 'app-add-meeting',
  templateUrl: './add-meeting.component.html',
  styleUrls: ['./add-meeting.component.css']
})
export class AddMeetingComponent implements OnInit {

  user: User[] = [];
  startDate: string;
  endDate: string;
  constructor(private router: Router, private meetingService: MeetingService) { }

  ngOnInit() {
  }
  addMeeting(startDate: string, endDate: string) {
    this.meetingService.addMeeting(startDate, endDate);
  }
}
