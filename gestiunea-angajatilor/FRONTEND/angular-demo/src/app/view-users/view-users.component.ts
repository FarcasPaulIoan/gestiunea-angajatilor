import {Component, OnInit} from '@angular/core';
import {User} from '../model/user';
import {UserService} from '../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Injectable} from '@angular/core';


@Component({
  selector: 'app-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  users: any = [];
  displayedColumns: string[] = ['id', 'name', 'type', 'address', 'birthDate', 'emailAddress', 'delete', 'edit'];


  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
      console.log(data);
    });
  }

  deleteUser(id: number) {
    console.log(id);
    const itemIndex = this.users.findIndex(obj => obj[0] === id );
    this.users.splice(itemIndex, 1);
    console.log(this.users.length);
    this.userService.deleteUser(id);
    setTimeout(() => {
      window.location.reload();
    }, 100); // Activate after 5 minutes.

  }

  editUser(id: number) {
    localStorage.removeItem('editUserId');
    localStorage.setItem('editUserId', id.toString());
    this.router.navigate(['edit-user']);
  }

  addUser(): void {
    this.router.navigate(['add-user']);
  }


}


