import {Component, OnInit} from '@angular/core';
import {User} from '../model/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users_new.component.html',
  styleUrls: ['./users_new.component.css']
})

export class Users_newComponent implements OnInit {

  userId: string;
  users: User[] = [];
  displayedColumns: string[] = ['id', 'username', 'password', 'tip'];
  userName: string;
  userPassword: string;
  userType: string;
  userAddress: string;
  bday: string;
  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {

    this.userService.getUsers_id('0')
      .subscribe(data => {

        this.users = data;

      });
  }

  viewDetails(id: string): void {
  console.log('button press');
    // @ts-ignore
    this.userService.getUsers_id(id)
      .subscribe(data => {
        this.users = data;

        console.log('AAAAAA' + this.users.length);
      });
  }

  updateUserr(id: string, uN: string, uP: string, uT: string, uA: string, bD: string){
    console.log(id + ' ' + uN + ' ' + uP + ' ' + uT);
          this.userService.updateUser(id, uN, uP, uT, uA, bD);
  }

}
