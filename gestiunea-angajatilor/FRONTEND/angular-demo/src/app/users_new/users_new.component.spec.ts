import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {Users_newComponent} from './users_new.component';

describe('Users_newComponent', () => {
  let component: Users_newComponent;
  let fixture: ComponentFixture<Users_newComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Users_newComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Users_newComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
