import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Meeting} from '../model/meeting';
import {Feedback} from '../model/feedback';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  meetingsURL = 'http://localhost:8080/spring_demo_war/meeting/all';
  add_meetingURL = 'http://localhost:8080/spring_demo_war/meeting/insert';

  constructor(private http: HttpClient) {
  }

  getMeetings() {
    return this.http.get<Meeting[]>(this.meetingsURL);

  }

  addMeeting(startDate: string, endDate: string) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    let meeting: Meeting;
    meeting = new Meeting();
    meeting.startDate = startDate;
    meeting.endDate = endDate;

    return this.http.post<Meeting>(this.add_meetingURL, meeting, httpOptions ).subscribe();


  }

}
