import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
// @ts-ignore
import {Feedback} from '../model/feedback';
import {User} from '../model/user';
import {UsersComponent} from '../users/users.component';
import {LoginService} from './login.service';
import {FeedbackComponent} from '../feedback/feedback.component';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  feedbackURL = 'http://localhost:8080/spring_demo_war/feedback';
  add_feedbackURL = 'http://localhost:8080/spring_demo_war/feedback/insert';
  feedbacks: any = [];
  nr_feedbacks: number;
  user = 0;
  constructor(private http: HttpClient, private loginService: LoginService) {
  }


  getFeedbackByUser() {
    this.feedbacks = this.http.get<Feedback[]>(this.feedbackURL + '/' + this.loginService.getCurrentUser().id.toString());
    return this.feedbacks;
  }
   getUserId() {
    if (this.user === 0) {
      return 0;
    } else {
      return 1;
    }
  }
  getNrFeedbacks() {
    this.feedbacks = this.http.get<Feedback[]>(this.feedbackURL + '/' + this.loginService.getCurrentUser().id.toString());
    this.getFeedbackByUser().subscribe(data => {
      this.feedbacks = data;
      this.nr_feedbacks = data.length;
    });
    console.log(this.nr_feedbacks);
    return this.nr_feedbacks;
  }
  getFeedback(n: string) {
    return this.http.get<Feedback[]>(this.feedbackURL + '/' + n);

  }

  addFeedback(userId: string, text: string, reviewerId: string) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    let feedback: Feedback;
    feedback = new Feedback();
    let r: User;
    let u: User;
    r = new User();
    u = new User();
    u.id = parseInt(userId, 10);
    r.id = parseInt(reviewerId, 10);


    feedback.userId = u;
    feedback.reviewerId = r;


    feedback.text = text;

    return this.http.post<Feedback>(this.add_feedbackURL , feedback , httpOptions ).subscribe();


  }

}
