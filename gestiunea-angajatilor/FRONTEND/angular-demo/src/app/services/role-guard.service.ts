import { Injectable }  from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { LoginService } from './login.service';

@Injectable()
export class GuardService implements CanActivate {

    constructor(public router: Router, private loginService: LoginService) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {

        const expectedRole = route.data.expectedRole;

        const currentUser = this.loginService.getCurrentUser();

        console.log(expectedRole, currentUser);

        if (currentUser !== null) {
            if (currentUser.type >= expectedRole) {
                return true;
            }
        }

        this.router.navigate(['']);
        return false;
    }
}
