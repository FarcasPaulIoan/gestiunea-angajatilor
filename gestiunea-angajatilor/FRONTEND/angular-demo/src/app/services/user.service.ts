import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersURL = 'http://localhost:8080/spring_demo_war/user';
  usersURL_id = 'http://localhost:8080/spring_demo_war/user/user_info';
  update_usersURL = 'http://localhost:8080/spring_demo_war/user/update_user';
  constructor(private http: HttpClient) {
  }

  getUsers_id(n: string) {


    return this.http.get<User[]>(this.usersURL_id + '/' + n);
  }

  getUsers() {
    return this.http.get<User[]>(this.usersURL + '/all');
  }

  getUserById(id: number) {
    return this.http.get<User>(this.usersURL + '/user_info/' + id);
  }

  deleteUser(id: number) {
    return this.http.delete(this.usersURL + '/delete/' + id).subscribe();
  }

  addUser(password: string, name: string, type: number, address: string, birthDate: string, emailAddress: string) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    let user: User;
    user = new User();
    user.password = password;
    user.name = name;
    user.type = type;
    user.address = address;
    user.birthDate = birthDate;
    user.emailAddress = emailAddress;
    console.log(user);
    return this.http.post<User>(this.usersURL + '/insert' , user, httpOptions ).subscribe();


  }

  editUser(id: number, password: string, name: string, type: number, address: string, birthDate: string, emailAddress: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    let user: User;
    user = new User();
    user.password = password;
    user.name = name;
    user.type = type;
    user.address = address;
    user.birthDate = birthDate;
    user.emailAddress = emailAddress;
    return this.http.put<User>(this.usersURL + '/edit/' + id, httpOptions).subscribe();
  }


  updateUser(id: string, uN: string, uP: string, uT: string, uA: string, bD: string) {
    let user: User;
    user = new User();
    user.id = parseInt(id, 10);
    user.name = uN;
    user.password = uP;
    user.type = parseInt(uT, 10);
    user.address = uA;
    user.birthDate = bD;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    console.log(this.update_usersURL );
    console.log(user );
    return this.http.post<User>(this.update_usersURL , user , httpOptions).subscribe();

  }
}
