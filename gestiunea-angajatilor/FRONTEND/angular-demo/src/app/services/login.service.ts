import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginURL = 'http://localhost:8080/spring_demo_war/user/login';

  constructor(private http: HttpClient) {
  }

  login(un: string, pw: string): Observable<User> {
    let user: User;
    user = new User();

    user.name = un;
    user.password = pw;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };

    const promise = this.http.post<User>(this.loginURL, user, httpOptions);

    promise.subscribe(data => {
      const foundUser = data;

      if (foundUser != null) {
        localStorage.setItem('currentUser', JSON.stringify(foundUser));
        return true;
      } else {
        this.logout();
        return false;
      }
    });

    return promise;
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
