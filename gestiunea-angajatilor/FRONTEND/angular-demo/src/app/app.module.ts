import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatIconModule, MatMenuModule, MatToolbarModule, MatButtonModule, MatTableModule} from '@angular/material';
import {RouterModule, Routes, CanActivate} from '@angular/router';
import {GuardService} from './services/role-guard.service';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ViewUsersComponent} from './view-users/view-users.component';
import {HomeComponent} from './home/home.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import { UsersComponent } from './users/users.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {MeetingComponent} from './meeting/meeting.component';
import {Users_newComponent} from './users_new/users_new.component';
import { ViewFeedbacksComponent } from './view-feedbacks/view-feedbacks.component';
import {ViewMeetingsComponent} from './view-meetings/view-meetings.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddMeetingComponent } from './add-meeting/add-meeting.component';
import { EditUserComponent } from './edit-user/edit-user.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewUsersComponent,
    HomeComponent,
    UserDetailsComponent,
    HeaderComponent,
    UsersComponent,
    FeedbackComponent,
    ViewMeetingsComponent,
    MeetingComponent,
    Users_newComponent,
    ViewFeedbacksComponent,
    AddUserComponent,
    AddMeetingComponent,
    EditUserComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'users',
        component: ViewUsersComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '1'
        }
      },
      {
        path: 'user/:id',
        component: UserDetailsComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '0'
        }
      },
      {
        path: 'add-user',
        component: AddUserComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '2'
        }
      },
      {
        path: 'edit-user',
        component: EditUserComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '2'
        }
      },
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'feedback',
        component: FeedbackComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '1'
        }
      },
      {
        path: 'feedbacks',
        component: ViewFeedbacksComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '0'
        }
      },
      {
        path: 'meeting',
        component: MeetingComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '0'
        }
      },
      {
        path: 'add-meeting',
        component: AddMeetingComponent,
        canActivate: [GuardService],
        data: {
          expectedRole: '2'
        }
      },
      {
        path: 'users_new',
        component: Users_newComponent,
        data: {
          expectedRole: '2'
        }
      },
      {
        path: '**',
        redirectTo: ''
      }
    ])
  ],
  providers: [FeedbackComponent, GuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
