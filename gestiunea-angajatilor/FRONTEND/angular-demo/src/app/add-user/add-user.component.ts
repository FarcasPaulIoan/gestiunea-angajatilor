import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  password: string;
  name: string;
  type: number;
  address: string;
  birthDate: string;
  emailAddress: string;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.birthDate = new Date().toISOString().split('T')[0];
  }
  addUser(password: string, name: string, type: number, address: string, birthDate: string, emailAddress: string) {
    this.userService.addUser(password, name, type, address, birthDate, emailAddress);
  }

}
