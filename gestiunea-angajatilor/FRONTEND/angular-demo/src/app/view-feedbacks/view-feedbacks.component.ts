import {Component, OnInit} from '@angular/core';
import {Feedback} from '../model/feedback';
import {FeedbackService} from '../services/feedback.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {LoginService} from '../services/login.service';


@Component({
  selector: 'app-feedbacks',
  templateUrl: './view-feedbacks.component.html',
  styleUrls: ['./view-feedbacks.component.css']
})
export class ViewFeedbacksComponent implements OnInit {

  feedbacks: any = [];
  displayedColumns: string[] = ['id', 'reviewer', 'text'];

  constructor(private route: ActivatedRoute, private feedbackService: FeedbackService, private loginService: LoginService) {
  }

  ngOnInit() {
    this.feedbackService.getFeedbackByUser().subscribe(data => {
      this.feedbacks = data;
      console.log(data);
    });
  }



}


