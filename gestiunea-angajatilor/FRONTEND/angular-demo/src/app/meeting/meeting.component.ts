import {Component, OnInit} from '@angular/core';
import {Meeting} from '../model/meeting';
import {MeetingService} from '../services/meeting.service';
import {Router} from '@angular/router';

// @ts-ignore
@Component({
  selector: 'app-users',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit {

  meetings: Meeting[] = [];
  displayedColumns: string[] = ['id',  'startDate', 'endDate'];

  constructor(private router: Router, private meetingService: MeetingService) {
  }

  ngOnInit() {
    this.meetingService.getMeetings()
      .subscribe(data => {
        this.meetings = data;

      });
  }

  addMeeting(): void {
    this.router.navigate(['add-meeting']);
  }

}
