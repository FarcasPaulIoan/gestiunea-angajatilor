export class User {
  id: number;
  password: string;
  name: string;
  type: number;
  address: string;
  birthDate: string;
  emailAddress: string;
}
