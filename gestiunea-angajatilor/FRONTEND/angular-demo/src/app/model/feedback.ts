import {User} from './user';

export class Feedback {
  id: number;
  text: string;
  reviewerId: User;
  userId: User;
}
