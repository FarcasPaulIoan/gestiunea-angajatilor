export class Meeting {
  id: number;
  startDate: string;
  endDate: string;
}
