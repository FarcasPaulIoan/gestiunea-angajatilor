import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../services/login.service';
import { interval } from 'rxjs';
import {FeedbackService} from '../services/feedback.service';
import {FeedbackComponent} from '../feedback/feedback.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  hasNotifications: boolean;
  loggedIn: boolean;
  loggedInName: string;
  loggedInRole: string;
  feedbacks: any;
  initial_nr: number;
  nr_feedbacks: number;


  constructor(private router: Router, private loginService: LoginService, private feedbackService: FeedbackService,
              private fbComp: FeedbackComponent) {
  }

  ngOnInit() {
    this.initial_nr = this.feedbackService.getNrFeedbacks();

    interval(50).subscribe(_ => {
      if (this.loginService.getCurrentUser() != null) {
        this.loggedIn = true;
        this.loggedInName = this.loginService.getCurrentUser().name;
        this.loggedInRole = this.userTypeToString(this.loginService.getCurrentUser().type);
      } else {
        this.loggedIn = false;
        this.loggedInName = 'Guest';
      }
    });
    interval(5000).subscribe(_ => {
      this.nr_feedbacks = this.feedbackService.getNrFeedbacks();
      console.log('FBC  ', Number(this.fbComp.user));
      if (this.nr_feedbacks !== this.initial_nr && this.loginService.getCurrentUser().id === this.feedbackService.getUserId() && this.feedbackService.getUserId() !== 0) {
        this.initial_nr = this.nr_feedbacks;
        console.log('NEW FEEDBACK');
        this.hasNotifications = true;
      }
    });
  }

  getUsers() {
    this.router.navigate(['users']);
  }

  getFeedbacks() {
    this.router.navigate(['feedbacks']);
    this.hasNotifications = false;
  }

  getMeetings() {
    this.router.navigate(['meeting']);
  }
  goBack() {
    this.router.navigate(['']);
  }

  logout() {
    this.router.navigate(['']);
    this.loginService.logout();
  }
  feedbackPage() {
    this.router.navigate(['feedback']);
  }

  userTypeToString(type: number) {
    if (type === 0) {
      return 'Angajat';
    } else if (type === 1) {
      return 'Manager';
    } else if (type === 2) {
      return 'HR';
    }
  }
}
