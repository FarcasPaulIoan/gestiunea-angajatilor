import {Component, OnInit} from '@angular/core';
// @ts-ignore
import {Feedback} from '../model/feedback';
import {FeedbackService} from '../services/feedback.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-users',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  feedback: Feedback[] = [];
  displayedColumns: string[] = ['id', 'userId', 'text', 'reviewerId'];
  employeeId: string;
  userId: string;
  text: string;
  reviewerId: string;
  user = 0;
  constructor(private router: Router, private feedbackService: FeedbackService) {
  }

  ngOnInit() {
    this.feedbackService.getFeedback('0')
      .subscribe(data => {
        this.feedback = data;
        console.log('AAAA');
      });
  }

  viewDetails(id: number): void {
    this.router.navigate(['feedback', id]);
  }

  list(n: string) {

    this.feedbackService.getFeedback(n)
      .subscribe(data => {
        this.feedback = data;
        console.log(this.feedback);
      });

  }

  addFeedback(userId: string, text: string, reviewerId: string) {
        this.feedbackService.addFeedback(userId, text, reviewerId);
        this.feedbackService.user = Number(userId);
  }


}
