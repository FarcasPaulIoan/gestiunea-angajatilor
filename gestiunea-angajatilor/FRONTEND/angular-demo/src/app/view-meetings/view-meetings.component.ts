import {Component, OnInit} from '@angular/core';
import {Meeting} from '../model/meeting';
import {MeetingService} from '../services/meeting.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Injectable} from '@angular/core';


@Component ({
  selector: 'app-meetings',
  templateUrl: './view-meetings.component.html',
  styleUrls: ['./view-meetings.component.css']
})
export class ViewMeetingsComponent implements OnInit {

  meetings: any = [];
  users: any = [];
  names = '';
  displayedColumns: string[] = ['id', 'users', 'startDate', 'endDate'];

  constructor(private route: ActivatedRoute, private meetingService: MeetingService) {
  }

  ngOnInit() {
    this.meetingService.getMeetings().subscribe(data => {
      this.meetings = data;
      let usernames = '';

      this.names = this.names + this.meetings.map( function (meeting) {
        meeting.users.map(function (user) {
          usernames = usernames + user.name + ' ';
        });
        return usernames;
      });
    });
  }



}


