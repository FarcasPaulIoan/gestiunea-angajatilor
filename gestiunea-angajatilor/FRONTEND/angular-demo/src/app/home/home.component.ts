import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { User } from '../model/user';
import { Logs } from 'selenium-webdriver';
import { interval } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'Gestiunea Angajatilor';
  username: string;
  password: string;
  loggedIn: boolean;

  constructor(private loginService: LoginService) {
  }

  ngOnInit() {
    interval(50).subscribe(_ => {
      if (this.loginService.getCurrentUser() != null) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }

  login(un: string, pw: string) {
    this.loginService.login(un, pw).subscribe(data => {
      if (data != null) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
        alert('User not found or incorrect password!');
      }
    });
  }
}
