package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Meeting;

public interface MeetingRepository extends JpaRepository<Meeting, Integer> {

    Meeting findById(int id);

}