package spring.demo.services;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.entities.Feedback;
import spring.demo.repositories.FeedbackRepository;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

@Service
public class FeedbackService {

    @Autowired
    private FeedbackRepository feedbckRepository;

    public Feedback findFeedbackById(int feedbackId) {
        Feedback feedbck = feedbckRepository.findById(feedbackId);
        return feedbck;
    }

    public List<Feedback> findAll() {
        List<Feedback> feedbacks = feedbckRepository.findAll();
        return feedbacks;
    }

    public int create(Feedback feedback) {
        Feedback feedbck = feedbckRepository.save(feedback);

        final String username = "gestiunea.angajatilor@gmail.com";
        final String password = "proiectdisi1";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("gestiunea.angajatilor@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(feedback.getUserId().getEmailAddress())
            );
            message.setSubject("New Feedback to " + feedback.getUserId().getName());
            message.setText("Feedback from " + feedback.getReviewerId().getName() + ": "
                    + "\n\n " + feedback.getText());

            Transport.send(message);

            System.out.println("Mail sent!");

        } catch (MessagingException e) {
            e.printStackTrace();
        }


        return feedbck.getId();
    }

    public ArrayList<Feedback> feedbackUserByUserId(int userId) {
        ArrayList<Feedback> result = new ArrayList<Feedback>();
        List<Feedback> allFeedback = feedbckRepository.findAll();
        for(Feedback feedback : allFeedback){
            if (feedback.getUserId().getId() == userId) {
                result.add(feedback);
            }
        }
        return result;
    }



}
