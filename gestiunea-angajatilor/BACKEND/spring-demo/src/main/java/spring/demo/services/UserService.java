package spring.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository usrRepository;

	public void creareUser(User user) {
		usrRepository.save(user);
	}


	public void stergereUserbyId(int id) {
		usrRepository.delete(id);
	}

	public List<User> informatiiUserById(int id) {

		List<User> u = new ArrayList<User>();
		u.add(usrRepository.findById(id));
		return u;
	}

	public List<User> getAllUsers(){
		return usrRepository.findAll();
	}

	public boolean updateUser(int id, User newUser) {
		User existingUser = usrRepository.findById(id);

		if(existingUser == null) {
			return false;
		}

		if(newUser.getName() != null) {
			existingUser.setName(newUser.getName());
		}

		if(newUser.getPassword() != null) {
			existingUser.setPassword(newUser.getPassword());
		}

		if(newUser.getType() != null) {
			existingUser.setType(newUser.getType());
		}

		if(newUser.getAddress() != null) {
			existingUser.setAddress(newUser.getAddress());
		}

		if(newUser.getBirthDate() != null) {
			existingUser.setBirthDate(newUser.getBirthDate());
		}

		usrRepository.save(existingUser);
		return true;
	}

	public boolean updateUser_new(User newUser) {
		System.out.println(newUser.getId()
				+ " " + newUser.getName()
				+ " " + newUser.getType()
				+ " " + newUser.getAddress()
				+ " " + newUser.getBirthDate()
				+ " " + newUser.getName()
				+ " " + newUser.getPassword());
		usrRepository.save(newUser);
		return true;
	}
}
