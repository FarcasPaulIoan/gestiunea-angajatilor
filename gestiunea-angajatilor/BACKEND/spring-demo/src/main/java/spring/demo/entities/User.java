package spring.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

	private static final long serialVersionUID = 2L;

	private Integer id;
	private String name;
	private String password;
	private Integer type;		// 0 - angajat, 1 - manager, 2 - hr (admin)
	@JsonIgnore
	private List<Meeting> meetings;
	@JsonIgnore
	private List<Feedback> receivedFeedbacks;
	@JsonIgnore
	private List<Feedback> givenFeedbacks;
	private String emailAddress;

	private String address;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date birthDate;

	public User() {
	}

	public User(Integer id, String name, String password, Integer type, String address, Date birthDate) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.type = type;
		this.meetings = new ArrayList<>();
		this.receivedFeedbacks = new ArrayList<>();
		this.givenFeedbacks = new ArrayList<>();
		this.address = address;
		this.birthDate = birthDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 200)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "password", nullable = false, length = 200)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "type", nullable = false)
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@ManyToMany(mappedBy = "users", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	public List<Meeting> getMeetings() {
		return meetings;
	}
	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}

	@Column(name = "address", length = 200)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "birth_date", nullable = false)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	@OneToMany(mappedBy = "userId", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	public List<Feedback> getReceivedFeedbacks() {
		return receivedFeedbacks;
	}
	public void setReceivedFeedbacks(List<Feedback> feedbacks) {
		this.receivedFeedbacks = feedbacks;
	}
	@OneToMany(mappedBy = "reviewerId", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	public List<Feedback> getGivenFeedbacks() {
		return givenFeedbacks;
	}

	public void setGivenFeedbacks(List<Feedback> givenFeedbacks) {
		this.givenFeedbacks = givenFeedbacks;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return name.equals(user.name) &&
				password.equals(user.password) &&
				type.equals(user.type) &&
				Objects.equals(address, user.address) &&
				birthDate.equals(user.birthDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, password, type, address, birthDate);
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", password='" + password + '\'' +
				", type=" + type +
				", emailAddress='" + emailAddress + '\'' +
				", address='" + address + '\'' +
				", birthDate=" + birthDate +
				'}';
	}
}
