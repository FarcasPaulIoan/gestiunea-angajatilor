package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.*;

@Entity
@Table(name = "feedback")
public class Feedback implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private User userId;
    private String text;
    private User reviewerId;

    public Feedback() {
    }

    public Feedback(Integer id, User userId, String text, User reviewerId) {
        super();
        this.id = id;
        this.userId = userId;
        this.text = text;
        this.reviewerId = reviewerId;
    }


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Column(name = "text", nullable = false, length = 200)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public User getReviewerId() {
        return reviewerId;
    }
    public void setReviewerId(User reviewerId) {
        this.reviewerId = reviewerId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }


    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                ", reviewerId=" + reviewerId +
                '}';
    }
}
