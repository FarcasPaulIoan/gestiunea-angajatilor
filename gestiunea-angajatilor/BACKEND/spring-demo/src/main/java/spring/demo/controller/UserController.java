package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import spring.demo.entities.User;
import spring.demo.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public void insertUser(@RequestBody User user) {
		user.setId(9999);
		userService.creareUser(user);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void stergereUserbyId(@PathVariable("id") int id) {
		userService.stergereUserbyId(id);
	}

	@RequestMapping(value = "/user_info/{id}", method = RequestMethod.GET)
	public List<User> informatiiUserById(@PathVariable("id") int id) {
		return userService.informatiiUserById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<User> getAllUsers(){
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<?> editUser(@PathVariable("id") int id, @RequestBody User user) {

		boolean result = userService.updateUser(id, user);

		if (!result) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok().body("User has been updated successfully.");
	}

	@RequestMapping(value = "/update_user", method = RequestMethod.POST)
	public boolean editUser_new(@RequestBody User user) {
		System.out.println(user.getId()
				+ " " + user.getName()
				+ " " + user.getType()
				+ " " + user.getAddress()
				+ " " + user.getBirthDate()
				+ " " + user.getName()
				+ " " + user.getPassword());
		userService.updateUser_new(user);
		return true;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public User login(@RequestBody User lookForUser) {
		List<User> users = userService.getAllUsers();

		for (User u : users) {
			if (u.getName().equals(lookForUser.getName()) && u.getPassword().equals(lookForUser.getPassword())) {
				return u;
			}
		}

		return null;
	}
}
