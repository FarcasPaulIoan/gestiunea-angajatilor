package spring.demo.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.demo.entities.Meeting;
import spring.demo.repositories.MeetingRepository;

@Service
public class MeetingService {

    @Autowired
    private MeetingRepository meetingRepository;


    public void addMeeting(Meeting meeting){
        meetingRepository.save(meeting);
    }

    public Meeting getMeetingById(int id) {
        return meetingRepository.findById(id);
    }

    public List<Meeting> getAllMeetings() { return meetingRepository.findAll(); }
}
