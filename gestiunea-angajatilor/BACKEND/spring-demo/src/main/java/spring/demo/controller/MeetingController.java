package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.entities.Meeting;
import spring.demo.services.MeetingService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/meeting")
public class MeetingController {
    @Autowired
    private MeetingService meetingService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public void insertUser(@RequestBody Meeting meeting) {
        meetingService.addMeeting(meeting);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Meeting getMeetingById(@PathVariable("id") int id) {
        return meetingService.getMeetingById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Meeting> getAllMeetings(){
        return meetingService.getAllMeetings();
    }
}