package spring.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.entities.Feedback;
import spring.demo.entities.User;
import spring.demo.services.FeedbackService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/feedback")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public void insertFeedback(@RequestBody Feedback feedbck) {
        feedbackService.create(feedbck);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public List<Feedback> feedbackUserByUserId(@PathVariable("id") int id) {
        return feedbackService.feedbackUserByUserId(id);
    }
}