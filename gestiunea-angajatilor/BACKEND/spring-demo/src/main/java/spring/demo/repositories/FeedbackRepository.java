package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.demo.entities.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {

    Feedback findById(int id);

}